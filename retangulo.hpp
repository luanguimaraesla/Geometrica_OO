#ifndef RETANGULO_H
#define RETANGULO_H

#include "geometrica.hpp"

class Retangulo : public Geometrica{
	public:
		Retangulo();
		Retangulo(float altura, float base);
		float area();
		float area(float altura, float base);
};

#endif
