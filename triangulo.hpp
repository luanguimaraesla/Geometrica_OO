#ifndef TRIANGULO_H
#define TRIANGULO_H

#include "geometrica.hpp"

class Triangulo : public Geometrica{
	public:
		Triangulo();
		Triangulo(float altura, float base);
		float area();
		float area(float altura, float base);
};

#endif
