#include "retangulo.hpp"

using namespace std;

Retangulo::Retangulo(){
	setBase(10);
	setAltura(10);
}

Retangulo::Retangulo(float altura, float base){
	setBase(base);
	setAltura(altura);
}

float Retangulo::area(){
	return getAltura() * getBase();
}

float Retangulo::area(float altura, float base){
	return altura * base;
}


