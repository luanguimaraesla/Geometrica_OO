#include "triangulo.hpp"
#include "retangulo.hpp"

using namespace std;

int main(){

	Triangulo *umTriangulo = new Triangulo(50, 20);
	Retangulo umRetangulo;

	cout << "A area do triangulo: " << umTriangulo->area() << endl;
	cout << "A area do retangulo: " << umRetangulo.area() << endl;

	return 0;
}
