#ifndef GEOMETRICA_H
#define GEOMETRICA_H

#include <iostream>

using namespace std;

class Geometrica{
	private:
		float altura, base;
	public:
		Geometrica();
		Geometrica(float altura, float base);
		float getAltura();
		float getBase();
		void setAltura(float altura);
		void setBase(float base);

		virtual float area() = 0;
};


#endif
